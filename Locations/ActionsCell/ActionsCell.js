import React, {useCallback} from 'react';
import { Button } from 'antd';

import './ActionsCell.scss';

const ActionsCell = (props) => {
  const {
    record,
    changeIsOpenDeleteLocationModal,
    changeIsOpenEditLocationModal,
    changeEditLocationTitle,
  } = props;
  const { id, title } = record;

  const handleDeleteLocation = () => changeIsOpenDeleteLocationModal(id);
  const handleEditLocation = useCallback(() => {
    changeEditLocationTitle(title);
    changeIsOpenEditLocationModal(id);
  }, [title, id]);

  return (
    <div className="actionsCell">
      <Button
        type="link"
        size="small"
        onClick={handleEditLocation}
        ghost
      >
        Edit
      </Button>
      <Button
        type="link"
        size="small"
        onClick={handleDeleteLocation}
        danger
      >
        Delete
      </Button>
    </div>
  );
};

export default ActionsCell;
