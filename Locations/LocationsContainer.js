/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { createComponent } from 'effector-react';

import Locations from './Locations';
import {
  $isLoading,
  $locations,
  $pagination,
  changePagination,
  fetchLocations,
  $search,
  changeSearch,
  $disabledAddNewLocation,
  addNewLocation,
  deleteLocation,
  $isOpenDeleteLocationModal,
  changeIsOpenDeleteLocationModal,
  $locationTitle,
  $isOpenAddNewLocationModal,
  changeIsOpenAddNewLocationModal,
  $isOpenEditLocationModal,
  changeIsOpenEditLocationModal,
  $editLocationTitle,
  changeEditLocationTitle,
  editLocation,
} from './model';

const LocationsContainer = createComponent(
  {
    $isLoading,
    $locations,
    $pagination,
    $search,
    changeSearch,
    changePagination,
    fetchLocations,
    $disabledAddNewLocation,
    addNewLocation,
    deleteLocation,
    $isOpenDeleteLocationModal,
    changeIsOpenDeleteLocationModal,
    $locationTitle,
    $isOpenAddNewLocationModal,
    changeIsOpenAddNewLocationModal,
    $isOpenEditLocationModal,
    changeIsOpenEditLocationModal,
    $editLocationTitle,
    changeEditLocationTitle,
    editLocation,
  },
  (props, state) => {
    useEffect(() => {
      state.fetchLocations();
    }, []);

    return (
      <Locations
        {...props}
        isLoading={state.$isLoading}
        locations={state.$locations}
        pagination={state.$pagination}
        changePagination={state.changePagination}
        search={state.$search}
        changeSearch={state.changeSearch}
        disabledAddNewLocation={state.$disabledAddNewLocation}
        addNewLocation={state.addNewLocation}
        deleteLocation={state.deleteLocation}
        isOpenDeleteLocationModal={state.$isOpenDeleteLocationModal}
        changeIsOpenDeleteLocationModal={state.changeIsOpenDeleteLocationModal}
        locationTitle={state.$locationTitle}
        isOpenAddNewLocationModal={state.$isOpenAddNewLocationModal}
        changeIsOpenAddNewLocationModal={state.changeIsOpenAddNewLocationModal}
        isOpenEditLocationModal={state.$isOpenEditLocationModal}
        changeIsOpenEditLocationModal={state.changeIsOpenEditLocationModal}
        editLocationTitle={state.$editLocationTitle}
        changeEditLocationTitle={state.changeEditLocationTitle}
        editLocation={state.editLocation}
      />
    );
  },
);

export default LocationsContainer;
