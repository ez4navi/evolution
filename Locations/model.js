import {
  combine, createEffect, createEvent, createStore, forward,
} from 'effector';
import { createDebounce } from 'effector-debounce';
import { notification } from 'antd';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import find from 'lodash/find';

import { client } from '../../../helpers/http';
import { $currentUser } from '../../../models';

export const $isLoading = createStore(false);
export const $locations = createStore([]);
export const $someLocations = createStore([]);
export const $search = createStore('');
export const $someSearch = createStore('');
export const $isOpenDeleteLocationModal = createStore(false);
export const $isOpenAddNewLocationModal = createStore(false);
export const $isOpenEditLocationModal = createStore(false);
export const $selectedLocation = createStore(null);
export const $locationTitle = createStore('');
export const $editLocationTitle = createStore('');

export const $disabledAddNewLocation = combine(
  { $search, $locations },
  (data) => {
    const { $search: search, $locations: locations } = data;
    const isEmptySearch = isEmpty(search);
    const isSameTitle = locations?.filter((location) => location.title === search)?.length > 0;

    return !(!isEmptySearch && !isSameTitle);
  },
);
export const $pagination = createStore({
  total: null,
  current: 1,
  pageSize: 10,
  showSizeChanger: true,
  showQuickJumper: true,
  size: 'small',
});

export const fetchLocations = createEvent();
export const fetchSomeLocations = createEvent();
export const changeSearch = createEvent();
export const changeSomeSearch = createEvent();
export const changePagination = createEvent();
export const addNewLocation = createEvent();
export const clearSearch = createEvent();
export const deleteLocation = createEvent();
export const changeIsOpenDeleteLocationModal = createEvent();
export const changeIsOpenAddNewLocationModal = createEvent();
export const changeIsOpenEditLocationModal = createEvent();
export const changeLocationTitle = createEvent();
export const changeSelectedLocation = createEvent();
export const changeEditLocationTitle = createEvent();
export const editLocation = createEvent();

const changeSearchDebounce = createDebounce(changeSearch, 500);
const changeSomeSearchDebounce = createDebounce(changeSomeSearch, 500);

const _fetchLocations = createEffect();
const _fetchSomeLocations = createEffect();
const _createNewLocation = createEffect();
const _deleteLocation = createEffect();
const _editLocation = createEffect();

_fetchSomeLocations.use(() => {
  const options = {
    params: {
      ordering: 'title',
      limit: 10,
      search: $someSearch.getState(),
    },
  };

  return client
    .get('/api/v1/locations/', options)
    .then((response) => ({
      locations: get(response, 'data.results', []),
      count: get(response, 'data.count', null),
      ordering: 'title',
    }))
    .catch((error) => {
      const message = get(error, 'message', '');
      notification.error({ message });
    });
});

_fetchLocations.use(() => {
  const pagination = $pagination.getState();
  const search = $search.getState();
  const options = {
    params: {
      ordering: 'title',
      offset: (pagination.current - 1) * pagination.pageSize,
      limit: pagination.pageSize,
      search,
    },
  };

  return client
    .get('/api/v1/locations/', options)
    .then((response) => {
      const data = {
        locations: get(response, 'data.results', []),
        count: get(response, 'data.count', null),
        ordering: 'title',
      };

      return data;
    })
    .catch((error) => {
      const message = get(error, 'message', '');
      notification.error({ message });
    });
});

_createNewLocation.use(() => {
  const search = $search.getState();
  const options = {
    title: search,
    account: $currentUser.getState().account.id,
  };

  return client
    .post('/api/v1/locations/', options)
    .then(() => {
      notification.success({ message: 'Location was successfully created.' });
      clearSearch();
      fetchLocations();
      changeIsOpenAddNewLocationModal();
    })
    .catch((error) => {
      const message = get(error, 'message', '');
      notification.error({ message });
    });
});

_editLocation.use(() => {
  const selectedLocation = $selectedLocation.getState();
  const newTitle = $editLocationTitle.getState();

  const options = {
    title: newTitle,
  };

  return client
    .patch(`/api/v1/locations/${selectedLocation}/`, options)
    .then(() => {
      notification.success({ message: 'Location was successfully changed.' });
      fetchLocations();
      changeIsOpenEditLocationModal();
    })
    .catch((error) => {
      const message = get(error, 'response.data.title', null) || get(error, 'response.data.errors', null) || 'Something went wrong';
      notification.error({ message });
    });
});

_deleteLocation.use(() => {
  const selectedLocation = $selectedLocation.getState();

  return client
    .delete(`/api/v1/locations/${selectedLocation}/`)
    .then(() => {
      notification.success({ message: 'Location was successfully deleted.' });
      fetchLocations();
      changeIsOpenDeleteLocationModal();
    })
    .catch((error) => {
      const message = get(error, 'message', '');
      notification.error({message: error?.response?.data?.errors || error?.response?.data || message});
    });
});

$locationTitle.on(changeLocationTitle, (_, params) => {
  const locations = $locations.getState();
  const organization = find(locations, ['id', params]);
  const title = get(organization, 'title', '');

  return title;
});

$selectedLocation.on(changeSelectedLocation, (_, params) => params);
$editLocationTitle.on(changeEditLocationTitle, (_, params) => params);
$search.on(changeSearch, (_, params) => params).reset(clearSearch);
$someSearch.on(changeSomeSearch, (_, params) => params);
$locations.on(_fetchLocations.done, (_, { result }) => result.locations);
$someLocations.on(_fetchSomeLocations.done, (_, { result }) => result.locations);
$isOpenDeleteLocationModal.on(changeIsOpenDeleteLocationModal, (state) => !state);
$isOpenAddNewLocationModal.on(changeIsOpenAddNewLocationModal, (state) => !state);
$isOpenEditLocationModal.on(changeIsOpenEditLocationModal, (state) => !state);

forward({
  from: changeIsOpenDeleteLocationModal,
  to: $selectedLocation,
});

changeIsOpenEditLocationModal
  .watch((params) => {
    changeSelectedLocation(params);
    changeLocationTitle(params);
  });

forward({
  from: $locationTitle,
  to: $editLocationTitle,
});

fetchLocations.watch(() => _fetchLocations());
changePagination.watch(() => _fetchLocations());
changeSearchDebounce.watch(() => _fetchLocations());
addNewLocation.watch(() => _createNewLocation());
deleteLocation.watch(() => _deleteLocation());
editLocation.watch(() => _editLocation());
fetchSomeLocations.watch(() => _fetchSomeLocations());
changeSomeSearchDebounce.watch(() => _fetchSomeLocations());

$pagination
  .on(changePagination, (_, params) => params)
  .on($search, (state) => ({
    ...state,
    current: 1,
  }))
  .on(_fetchLocations.done, (state, { result }) => ({
    ...state,
    total: result.count,
  }));

$isLoading
  .on(_fetchLocations, () => true)
  .on(_fetchLocations.done, () => false)
  .on(_fetchLocations.fail, () => false)
  .on(_fetchSomeLocations, () => true)
  .on(_fetchSomeLocations.done, () => false)
  .on(_fetchSomeLocations.fail, () => false)
  .on(_deleteLocation, () => true)
  .on(_deleteLocation.done, () => false)
  .on(_deleteLocation.fail, () => false)
  .on(_editLocation, () => true)
  .on(_editLocation.done, () => false)
  .on(_editLocation.fail, () => false);
