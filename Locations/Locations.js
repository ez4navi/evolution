/* eslint-disable react-hooks/exhaustive-deps */
import React, { useMemo, useEffect } from 'react';
import {
  Input, Table, Modal, Form,
} from 'antd';

import { Button } from '../../../components';
import { validateMessages, maxLength } from '../../../helpers/constants';

import ActionsCell from './ActionsCell';

import './Locations.scss';

const Locations = (props) => {
  const [form] = Form.useForm();

  const {
    isLoading,
    locations,
    pagination,
    changePagination,
    search,
    changeSearch,
    disabledAddNewLocation,
    addNewLocation,
    deleteLocation,
    isOpenDeleteLocationModal,
    changeIsOpenDeleteLocationModal,
    isOpenAddNewLocationModal,
    changeIsOpenAddNewLocationModal,
    isOpenEditLocationModal,
    changeIsOpenEditLocationModal,
    editLocationTitle,
    changeEditLocationTitle,
    editLocation,
  } = props;

  useEffect(() => {
    form.setFieldsValue({
      editLocationTitle,
    })
  }, [editLocationTitle]);

  const columns = useMemo(() => [
    {
      key: 'fullName',
      title: 'Location',
      ellipsis: true,
      render: (_, record) => (
        // <div onClick={() => { setCurrentRecord(record); }} className={classnames({ locationsTable_row: true, active: record?.id == currentRecord?.id })}>
        <span>
          {record.title}
        </span>
        // </div>
      ),
    },
    {
      key: 'actions',
      title: '',
      width: 150,
      render: (_, record) => (
        <ActionsCell
          record={record}
          changeEditLocationTitle={changeEditLocationTitle}
          changeIsOpenDeleteLocationModal={changeIsOpenDeleteLocationModal}
          changeIsOpenEditLocationModal={changeIsOpenEditLocationModal}
        />
      ),
    },
  ], [locations]);

  const handleChangeSearch = (event) => changeSearch(event.target.value);

  return (
    <>
      <div className="locationsSearchContainer">
        <Input.Search
          allowClear
          className="locationsSearch"
          placeholder="Search location or enter new name"
          value={search}
          onChange={handleChangeSearch}
          maxLength={maxLength}
        />

        <Button
          type="primary"
          className="locationsAddNewButton"
          disabled={disabledAddNewLocation}
          onClick={changeIsOpenAddNewLocationModal}
        >
          Add New Location
        </Button>
      </div>
      <div className="locationsTable">
        <Table
          rowKey="id"
          columns={columns}
          dataSource={locations}
          pagination={pagination}
          onChange={changePagination}
          loading={isLoading}
        />
      </div>

      <Modal
        title="Delete Location"
        width={500}
        visible={isOpenDeleteLocationModal}
        onCancel={() => changeIsOpenDeleteLocationModal(null)}
        onOk={deleteLocation}
        cancelButtonProps={{ style: { display: 'none' } }}
        okText="Confirm"
        centered
      >
        Please confirm you want to delete this Location. Note that this is irreversible action.
      </Modal>

      <Modal
        title="Add Location"
        width={500}
        visible={isOpenAddNewLocationModal}
        onCancel={() => changeIsOpenAddNewLocationModal()}
        onOk={addNewLocation}
        cancelButtonProps={{ style: { display: 'none' } }}
        okText="Confirm"
        centered
      >
        Please confirm adding a new location:
        <br />
        <b>{search}</b>
      </Modal>

      <Modal
        title="Edit Location"
        width={500}
        visible={isOpenEditLocationModal}
        onCancel={() => changeIsOpenEditLocationModal()}
        onOk={editLocation}
        cancelButtonProps={{ style: { display: 'none' } }}
        okButtonProps={{ disabled: !editLocationTitle || editLocationTitle.length > 64 }}
        okText="Confirm"
        centered
      >
        Please change location title:
        <br />
        {isOpenEditLocationModal && (
          <Form
            onValuesChange={(changedValues) => {
              changeEditLocationTitle(changedValues.editLocationTitle);
            }}
            form={form}
            validateMessages={validateMessages}
          >
            <Form.Item
              name="editLocationTitle"
              rules={[
                {
                  required: true,
                  max: maxLength,
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        )}
      </Modal>
    </>
  );
};

export default Locations;
